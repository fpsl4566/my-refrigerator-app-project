import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/model/order_of_cooking_item.dart';

class ComponentsOrderOfCooking extends StatelessWidget {
  const ComponentsOrderOfCooking(
      {super.key,
        required this.indexOfCooking,
        required this.orderOfCookingItem,
        required this.callback
      }
     );

  final List indexOfCooking;
  final OrderOfCookingItem orderOfCookingItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: callback,
        child: Padding(

          padding: const EdgeInsets.all(12.0),
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: [
                Container(
                    margin: EdgeInsets.fromLTRB(5, 0, 20, 5),
                    child: Text("${indexOfCooking}",
                      softWrap: true
                    )),
                SizedBox(width: MediaQuery.of(context).size.width/1.4,
                  child: Text("${orderOfCookingItem.recipe}",
                  softWrap: true,
                  style: const TextStyle(fontSize: 12.0, fontWeight: FontWeight.w700, fontFamily: 'palatino'),),
                ),
              ],
            ),
          ),
        ),

    );
  }
}

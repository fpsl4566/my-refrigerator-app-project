import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/model/food_ingredient_item.dart';
import 'package:my_refrigerator_app/model/order_of_cooking_item.dart';

class ComponentsSearchFood extends StatelessWidget {
  const ComponentsSearchFood({
    super.key,
    required this.foodIngredientItem,
    required this.callback
  });

  final FoodIngredientItem foodIngredientItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Card(
        elevation: 0.5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),

        child: Container(
          child: Column(
            children: [
              Text("${foodIngredientItem.foodName}"),
              Text("${foodIngredientItem.refrigeratorFood}"),
            ],
          ),
        ),
      ),
    );
  }
}

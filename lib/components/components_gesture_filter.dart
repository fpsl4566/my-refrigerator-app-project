import 'package:flutter/material.dart';

class ComponentsGestureFilter extends StatefulWidget {
  const ComponentsGestureFilter({super.key,required this.callback});
  final VoidCallback callback;



  @override
  State<ComponentsGestureFilter> createState() =>
      _ComponentsGestureFilterState();
}
void _handleTap() {
  // 탭(클릭) 제스처가 감지될 때 실행될 코드
  print('Tap gesture detected!');

  void _handleDoubleTap() {
    // 더블 탭 제스처가 감지될 때 실행될 코드
    print('Double tap gesture detected!');
  }
  void _handleLongPress() {
    // 길게 누르는 제스처가 감지될 때 실행될 코드
    print('Long press gesture detected!');
  }

}
class _ComponentsGestureFilterState extends State<ComponentsGestureFilter> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(''),
        ),
        body: Center(
          child: GestureDetector(
            onTap: () {

              print('');
            },
            child: Text(
              '여기를 탭하세요',
              style: TextStyle(fontSize: 24),
            ),
          ),
        ),
      ),
    );
  }
}

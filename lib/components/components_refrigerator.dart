import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/model/food_Basket_item_result.dart';
import 'package:my_refrigerator_app/model/food_Basket_list_result.dart';

class ComponentsRefrigerator extends StatefulWidget {
  const ComponentsRefrigerator(
      {super.key, required this.foodBasketItemResult, required this.callback});

  final FoodBasketItemResult foodBasketItemResult;
  final VoidCallback callback;

  @override
  State<ComponentsRefrigerator> createState() => _ComponentsRefrigeratorState();
}

bool _isCheckRed = false;
bool _isCheckOrange = false;
bool _isCheckGreen = false;
bool _isCheckBlue = false;

List<String> checkList = [];

class _ComponentsRefrigeratorState extends State<ComponentsRefrigerator> {
  bool? _isCheckRed = false;

  List<String> checkList = [];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.callback,
      child: SingleChildScrollView(
        child: Row(
          children: [
            Checkbox(
              value: _isCheckRed,
              onChanged: (value) {
                setState(() {
                  _isCheckRed = value;
                });
              },
            ),
            Text(widget.foodBasketItemResult.refrigeratorFood),
            SizedBox(
              height: 50,
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/model/food_name_item.dart';



class ComponentsFoodName extends StatelessWidget {
  const ComponentsFoodName({
    super.key,
    required this.foodNameItem,
    required this.callback

  });

  final FoodNameItem foodNameItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Card(
        color: Colors.white,
        elevation: 0.5,
        borderOnForeground: true,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0.0)),

        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Text(foodNameItem.foodName,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
              ),
              Text(foodNameItem.calorie),
              Text(foodNameItem.dateRegister),
              SizedBox(height: 10,),
            ],
          ),
        ),
      ),
    );


  }
}

import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

class ComponentsListBarNormal extends StatefulWidget {
  const ComponentsListBarNormal({
    super.key,
    required this.List,


  });

  final String List;


  @override
  State<ComponentsListBarNormal> createState() =>
      _ComponentsListBarNormalState();
}

final List<String> items = [
  '회원가입',
  '레시피문의',
  '연결에러',
  '광고문의',
];

String? selectedValue;

class _ComponentsListBarNormalState extends State<ComponentsListBarNormal> { //상속받다 제네릭
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:(){},

      child:DropdownButtonHideUnderline(
        child: DropdownButton2<String>(
          isExpanded: true,
          hint: Text(
            '문의내용을 선택해주세요',
            style: TextStyle(
              fontSize: 14,
              color: Theme.of(context).hintColor,
            ),
          ),
          items: items
              .map((String item) => DropdownMenuItem<String>(
            value: item,
            child: Text(
              item,
              style: const TextStyle(
                fontSize: 14,
              ),
            ),
          ))
              .toList(),
          value: selectedValue,
          onChanged: (String? value) {
            setState(() {
              selectedValue = value;
            });
          },
          buttonStyleData: const ButtonStyleData(
            padding: EdgeInsets.symmetric(horizontal: 16),
            height: 50,
            width: 400,
          ),
          menuItemStyleData: const MenuItemStyleData(
            height: 40,
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/model/question_item.dart';
import 'package:my_refrigerator_app/pages/food/Page_order_of_cooking_index.dart';
import 'package:my_refrigerator_app/pages/food/page_food_imgs_index.dart';
import 'package:my_refrigerator_app/pages/food/index.dart';
import 'package:my_refrigerator_app/pages/page_comments_index.dart';
import 'package:my_refrigerator_app/pages/page_questions_list_index.dart';
import 'package:my_refrigerator_app/pages/food/page_refrigerator_food_index.dart';
import '../../pages/mainpage/page_change_information_index.dart';
import '../../pages/page_enquiry_registration_index.dart';



class ComponentsDrawerBarNormal extends StatefulWidget {
  const ComponentsDrawerBarNormal({
    super.key,

  });

  @override
  State<ComponentsDrawerBarNormal> createState() => _ComponentsDrawerBarNormalState();
}

class _ComponentsDrawerBarNormalState extends State<ComponentsDrawerBarNormal> {

  QuestionItem? _inquiryItem;

  @override
  Widget build(BuildContext context) {
    return Drawer(
        elevation: 1.0,
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            UserAccountsDrawerHeader(accountName: Text('*** 님 반갑습니다!'), accountEmail: Text('abc123@naner.com'),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("assets/img1.png"),
              backgroundColor: Color.fromRGBO(21, 94, 60,1.0),
            ),
            ),




            Theme(data: Theme.of(context).copyWith(
              dividerColor: Colors.transparent,
            ) , child:
            ListTile(
              leading: Icon(
                Icons.account_circle,
                  color: Color.fromRGBO(21, 94, 60,1.0),

              ),
              title: Text('마이페이지'),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => PageChangeInformationIndex()));

                print('Setting click');
              },
            ),),

            ListTile(
              leading: Icon(
                Icons.restaurant,
                color: Color.fromRGBO(21, 94, 60,1.0),

              ),
              title: Text('냉장고'),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PageRefrigeratorFoodIndex()));


                print('Setting click');
              },
            ),
            ListTile(
              leading: Icon(
                Icons.event_note,
                color: Color.fromRGBO(21, 94, 60,1.0),
              ),
              title: Text('레시피관리'),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PageFoodImgsIndex()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.playlist_add_check,
                color: Color.fromRGBO(21, 94, 60,1.0),
              ),
              title: Text('게시판'),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PageQuestionsListIndex()));
                print('Setting click');
              },
            ),
            ListTile(
              leading: Icon(
                Icons.subject,
                color: Color.fromRGBO(21, 94, 60,1.0),
              ),
              title: Text('문의사항'),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PageCommentsIndex()));
                print('Setting click');
              },
            ),
            ListTile(
              leading: Icon(
                Icons.account_circle,
                color: Color.fromRGBO(21, 94, 60,1.0),
              ),
              title: Text('이용약관'),
              onTap: (){
                print('Setting click');
              },
            ),
          ],
        ),
      );
  }
}

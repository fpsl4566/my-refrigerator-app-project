import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/model/refrigerator_food_item.dart';
import 'package:my_refrigerator_app/model/food_Basket_list_result.dart';

class ComponentsRefrigeratorFood extends StatelessWidget {
  const ComponentsRefrigeratorFood(
      {super.key,
      required this.refrigeratorFoodItem,
      required this.callback,
      required this.refrigeratorFoodImg});

  final RefrigeratorFoodItem refrigeratorFoodItem;
  final String refrigeratorFoodImg;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    width: 1,
                    color: Colors.black45,
                    style: BorderStyle.solid,
                  ),
                ),
              ),
              child: Row(
                children: [
                  Container(
                    height: 100,
                    width: 100,
                    child: Image.asset(
                      '${refrigeratorFoodImg}',
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(width: 100),
                  Text(
                    "${refrigeratorFoodItem.refrigeratorFood}",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/model/question_item.dart';

class ComponentsQuestionsItem extends StatelessWidget {
  const ComponentsQuestionsItem(
      {super.key, required this.questionItem, required this.callback});

  final QuestionItem questionItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: callback,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
                width: 1,
                color: Colors.black
            ),
          )
        ),
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width / 3,


          child: Column(
            children: [
              Row( mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 4,
                      child: Text("[${questionItem.questionCategory}]",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12
                  ),)),
                  Container( width: 150,
                      child: Text(questionItem.title,
                      style: TextStyle(
                        fontSize: 15
                      ),)),
                  Column(
                    children: [
                      Container(child: Text(questionItem.memberEntityId)),
                      Container(margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                          child: Text(questionItem.dateWrite,
                      style: TextStyle(
                        fontSize: 12
                      ),
                      )),
                    ],
                  ),
                ],
              ),
              //Text(questionItem.answerStatus),
            ],
          ),

      ),
    );
  }
}

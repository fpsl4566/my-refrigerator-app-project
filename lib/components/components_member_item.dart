import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/model/members_item.dart';

class ComponentsMemberItem extends StatelessWidget {
  const ComponentsMemberItem({
    super.key,
    required this.membersItem,
    required this.callback

  });

  final MembersItem membersItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Text(membersItem.memberName),
            Text(membersItem.memberId),
            Text(membersItem.password),
            Text(membersItem.phoneNumber),
            Text(membersItem.mail),
          ],
        ),
      ),
    );
  }
}

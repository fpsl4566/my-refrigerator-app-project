import 'package:flutter/material.dart';

const Color colorPrimary = Color.fromRGBO(0, 0, 0, 1); // 주 색상 opacity 투명도
const Color colorSecondary = Color.fromRGBO(241, 241, 241, 1.0);
const Color colorThird = Color.fromRGBO(150, 150, 150, 1.0);

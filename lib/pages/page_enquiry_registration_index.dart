import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/pages/page_comments_details_index.dart';
import 'package:my_refrigerator_app/pages/page_comments_index.dart';

import '../components/design/components_drawer_bar_normal.dart';

class PageEnquiryRegistrationIndex extends StatefulWidget {
  const PageEnquiryRegistrationIndex({super.key});

  @override
  State<PageEnquiryRegistrationIndex> createState() =>
      _PageEnquiryRegistrationIndexState();
}

class _PageEnquiryRegistrationIndexState
    extends State<PageEnquiryRegistrationIndex> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    Future<dynamic> _showdialog(BuildContext context) {
      return showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('문의사항'),
          content: const Text('내용을 등록하시겠습니까?'),
          actions: [
            ElevatedButton(
                onPressed: () => Navigator.of(context).pop(),
                child: const Text('취소')),
            ElevatedButton(
                onPressed: (
                    ) => Navigator.of(context).push(MaterialPageRoute(builder: (
                    context) => const PageCommentsDetailsIndex())),
                child: const Text('확인')),


          ],
        ),
      );
    }
    // 문의 사항 만든 페이지

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '1:1문의 작성',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        backgroundColor: const Color.fromRGBO(55, 115, 78, 0.99),
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.menu),
        ),
      ),
      drawer: const ComponentsDrawerBarNormal(),
      body: SingleChildScrollView(

        child: Container(
          margin: EdgeInsets.only(left: 10, right: 10, top: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                '문의내용 *',
                style: TextStyle(
                  fontSize: 22,
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                child: const TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: '제목',
                  ),
                  keyboardType: TextInputType.multiline,
                ),
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      labelText: '문의 내용을 입력해주세요.',
                      border: OutlineInputBorder(),
                    ),
                    maxLength: 5000,
                    maxLines: 8,
                    keyboardType: TextInputType.multiline,
                  ),
                ),
              ),
              Container(
                height: 40,
                child: const Text(
                  '사진',
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                color: Colors.grey,
                width: 150,
                height: 150,
                child: IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.add_circle_outline),
                ),
              ),
              Column(
                children: [
                  const SizedBox(height: 30,),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,

                    children: [
                      TextButton(onPressed: () {}, child: const Text('뒤로가기')),
                      TextButton(onPressed: () {
                        _showdialog(context);
                      }, child: const Text('등록하기')),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

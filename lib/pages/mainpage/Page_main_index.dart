import 'dart:async';
import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/pages/mainpage/page_login_index.dart';

class PageMainIndex extends StatefulWidget {
  const PageMainIndex({super.key});

  @override
  State<PageMainIndex> createState() => _PageMainIndexState();
}

class _PageMainIndexState extends State<PageMainIndex> {
  @override
  void initState() {
    Timer(Duration(milliseconds: 1550), () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => PageLoginIndex()),);
    });
  }
  //메인페이지

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(55, 115, 78, 0.99),
      appBar: AppBar(
        leading: IconButton(onPressed: () {}, icon: Icon(Icons.menu)),
        title: Text(' '),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Color.fromRGBO(55, 115, 78, 0.99),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 300,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'MY',
                      style: TextStyle(
                        fontSize: 45,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                        child: const Text(
                          'REFRIGATOR',
                          style: TextStyle(
                            fontSize: 45,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              height: 70,
              margin: EdgeInsets.only(top: 30),
              width: 130,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/model/members_item.dart';
import 'package:my_refrigerator_app/pages/mainpage/page_login_index.dart';
import 'package:my_refrigerator_app/repository/repo_product.dart';

class PageMainShopIndex extends StatefulWidget {
  const PageMainShopIndex({super.key});

  @override
  State<PageMainShopIndex> createState() => _PageMainShopIndexState();
}

class _PageMainShopIndexState extends State<PageMainShopIndex> {

  List<MembersItem> _list = [];

  Future<void> _loadList()async{
    await RepoProduct().getProduct(1)
        .then((res) => {
          setState(() {
            _list = res.list;
           })
        })
        .catchError((err) => {
          debugPrint(err)
        });
    }

    @override
    void initState(){
      super.initState();
      _loadList();
    }



  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingValue = (screenWidth) / 12;

    //회원가입 페이지

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '회원가입',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromRGBO(55, 115, 78, 0.99),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: paddingValue),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: const Text(
                  '회원가입을 위해 회원 정보를 입력하세요.',
                  style: TextStyle(
                      fontSize: 30,
                      color: Color.fromRGBO(55, 115, 78, 0.99),
                      fontWeight: FontWeight.bold),
                ),
              ),
              Row(
                children: [
                  Container(
                    width: 250,
                    child: const TextField(
                      decoration: InputDecoration(
                        labelText: '아이디',
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  SizedBox(
                    width: 80,
                    child: TextButton(
                      onPressed: () {},
                      child: Text('중복확인'),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 16.0,
              ),
              Container(
                width: 250,
                child: const Column(
                  children: [
                    TextField(
                      decoration: InputDecoration(
                        //텍스트 상자 만들기
                        labelText: '이름',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  Container(
                    width: 250,
                    child: const TextField(
                      decoration: InputDecoration(
                        labelText: '이메일',
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  SizedBox(
                    width: 60,
                    child: TextButton(
                      onPressed: () {},
                      child: Text('인증'),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 16.0,
              ),
              const Column(
                children: [
                  SizedBox(
                    width: 250,
                    child: TextField(
                      obscureText: true, // 비밀번호 적을 때 안보이도록
                      decoration: InputDecoration(
                        labelText: '비빌번호를 입력해주세요.',
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                ],
              ),
              const SizedBox(
                width: 250,
                child: Column(
                  children: [
                    TextField(
                      obscureText: true, // 비밀번호 적을 때 안보이도록
                      decoration: InputDecoration(
                        labelText: '비빌번호를 한번 더 입력해주세요.',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  Container(
                    width: 250,
                    child: const TextField(
                      decoration: InputDecoration(
                        labelText: '핸드폰 번호',
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  SizedBox(
                    width: 60,
                    child: TextButton(
                      onPressed: () {},
                      child: Text('인증'),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 16.0,
              ),
              Container(
                margin: EdgeInsets.all(30),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const PageLoginIndex()));
                      },
                      style: TextButton.styleFrom(
                        minimumSize: const Size(50, 50),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(1),
                        ),
                        backgroundColor:
                            const Color.fromRGBO(55, 115, 78, 0.99),
                      ),
                      child: const Text(
                        '가입하기',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

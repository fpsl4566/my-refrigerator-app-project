import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/components/design/components_drawer_bar_normal.dart';
import 'package:my_refrigerator_app/pages/food/index.dart';
import 'package:my_refrigerator_app/pages/mainpage/page_loginid_index.dart';

import 'Page_Password_index.dart';
import 'Page_membership_index.dart';

class PageLoginIndex extends StatefulWidget {
  const PageLoginIndex({super.key});

  @override
  State<PageLoginIndex> createState() => _PageLoginIndexState();
}
//로그인페이지

class _PageLoginIndexState extends State<PageLoginIndex> {


@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '로그인',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromRGBO(55, 115, 78, 0.99),
        centerTitle: true,
      ),

      body: SingleChildScrollView(
        child: Container(
          child: Container(
            margin: const EdgeInsets.all(10),
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.all(10),
                  width: 500,
                  child: const Text(
                    '우리집 냉장고에 \n오신 걸 환영합니다.',
                    style: TextStyle(
                      fontSize: 25,
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(50),
                  child: SizedBox(
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 25),
                          child: const TextField(
                            decoration: InputDecoration(labelText: '아이디(이메일)'),
                            keyboardType: TextInputType.emailAddress,
                          ),
                        ),
                        const TextField(
                          obscureText: true,
                          decoration: InputDecoration(labelText: '비밀번호'),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child: Center(
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const PageFoodNameIndex()));
                      },
                      style: TextButton.styleFrom(
                        minimumSize: const Size(500, 50),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0)),
                        backgroundColor:
                            const Color.fromRGBO(55, 115, 78, 0.99),
                      ),
                      child: const Text(
                        '로그인',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(30),
                  child: Container(
                    child: Center(
                      child: Column(
                        children: [
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop(
                                      const PageMainShopIndex());
                            },
                            style: TextButton.styleFrom(
                              foregroundColor: Colors.black45,
                            ),
                            child: const Text(
                              '회원가입',
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(15),
                            child: Column(
                              children: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const PageLoginIdIndex()));
                                  },
                                  style: TextButton.styleFrom(
                                    foregroundColor: Colors.black45,
                                  ),
                                  child: const Text(
                                    '아이디 찾기',
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            children: [
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) =>
                                          const PagePasswordIndex()));
                                },
                                style: TextButton.styleFrom(
                                  foregroundColor: Colors.black45,
                                ),
                                child: const Text(
                                  '비빌번호 찾기',
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

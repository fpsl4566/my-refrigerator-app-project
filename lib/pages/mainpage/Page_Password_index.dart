import 'package:flutter/material.dart';

class PagePasswordIndex extends StatefulWidget {
  const PagePasswordIndex({super.key});

  @override
  State<PagePasswordIndex> createState() => _PagePasswordIndexState();
}
//비밀번호 찾기 페이지

class _PagePasswordIndexState extends State<PagePasswordIndex> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(55, 115, 78, 0.99),
        title: Text(''),
        centerTitle: true,
        elevation: 0.0,

      ),
      body: SingleChildScrollView(
        child: Container(
          margin:EdgeInsets.all(20),
          child: Column(
            children: [
              Text('비빌번호찾기',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold
              ),
              ),
              Container(
                margin: EdgeInsets.all(20),
                child: Center(
                  child: Text('가입된 아이디(이메일)을 입력해주세요.\n휴대폰 인증번호를 통해\n비빌번호를 변경합니다.',//글자 줄 넘
                  style: TextStyle(
                    fontSize: 20,
                  ),
                    softWrap: true,
                    textAlign: TextAlign.center, //글자 가운데 정렬
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(20),
                child: Column(
                  children: [
                    TextField(
                      decoration: InputDecoration(
                        labelText: '아이디 (이메일)',
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 50,

                child: Column(
                  children: [
                    ElevatedButton(onPressed: ()
                    {

                    },
                      style: TextButton.styleFrom(
                        minimumSize: Size(450, 50),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)
                        ),
                        backgroundColor:  Color.fromRGBO(55, 115, 78,0.99),
                      ),
                      child: Text('휴대폰 인증하기',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white70,
                      ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      )
    );
  }
}

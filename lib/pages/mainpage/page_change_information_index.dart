import 'package:flutter/material.dart';

class PageChangeInformationIndex extends StatefulWidget {
  const PageChangeInformationIndex({super.key});

  @override
  State<PageChangeInformationIndex> createState() =>
      _PageChangeInformationIndexState();
}

class _PageChangeInformationIndexState
    extends State<PageChangeInformationIndex> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingValue = (screenWidth - 300) / 2.0;

  //개인정보수정페이지
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '내 정보 변경',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(55, 115, 78, 0.99),
      ),
      body: Container(
        margin: EdgeInsets.all(50),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('아이디'),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const TextField(
                  decoration: InputDecoration(
                    labelText: 'jinsu123',
                    border: OutlineInputBorder(),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      '이름',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    const TextField(
                      decoration: InputDecoration(
                        labelText: '박진수',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          '이메일',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                        const TextField(
                          decoration: InputDecoration(
                            labelText: 'jinsub@naver.com',
                            border: OutlineInputBorder(),
                          ),
                        ),
                        Container(
                          child: const Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '비밀번호변경',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            ElevatedButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                minimumSize: Size(450, 50),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(0),
                                ),
                                backgroundColor:
                                    Color.fromRGBO(55, 115, 78, 0.99),
                              ),
                              child: const Text(
                                '비빌번호 변경',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white70,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                '휴대폰 번호',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    width: 230,
                                    child: const TextField(
                                      decoration: InputDecoration(
                                        labelText: '010-2453-5534',
                                        border: OutlineInputBorder(),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  SizedBox(
                                    width: 60,
                                    child: TextButton(
                                      onPressed: () {},
                                      child: const Text('인증'),
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 16.0,
                              ),
                              Container(
                                alignment: Alignment.center,
                                child: Column(
                                  children: [
                                    ElevatedButton(
                                      onPressed: () {

                                      },
                                      style: TextButton.styleFrom(
                                        minimumSize: const Size(50, 50),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(1),
                                        ),
                                        backgroundColor: const Color.fromRGBO(
                                            55, 115, 78, 0.99),
                                      ),
                                      child: const Center(
                                        child: Text(
                                          '저장하기',
                                          style: TextStyle(
                                            color: Colors.white70,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class PageLoginIdIndex extends StatefulWidget {
  const PageLoginIdIndex({super.key});

  @override
  State<PageLoginIdIndex> createState() => _PageLoginIdIndexState();
}

class _PageLoginIdIndexState extends State<PageLoginIdIndex> {
  @override

  //아이디 찾기
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(55, 115, 78, 0.99),
        title: Text(''),
        centerTitle: true,
        elevation: 0.0,
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.menu),
        ),
      ),


      body: Container(
        margin: EdgeInsets.all(30),
        alignment: Alignment.centerLeft,
        child: Center(
          child: Column(
            children: [
              const Text(
                '아이디찾기',
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                margin: const EdgeInsets.all(50),
              ),
              Container(
                margin: const EdgeInsets.all(50),
                child: const Center(
                  child: Text(
                    '휴대폰 본인인증을 통해\n아이디(이메일)를 확인합니다', // 글자 줄 넘김
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    softWrap: true,
                    textAlign: TextAlign.center, //글자 가운데 정렬
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(30),
                child: Column(
                  children: [
                    ElevatedButton(
                      onPressed: () {},
                      style: TextButton.styleFrom(
                        minimumSize: Size(450, 50),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0)),
                        backgroundColor:
                            const Color.fromRGBO(55, 115, 78, 0.99),
                      ),
                      child: const Text(
                        '휴대폰 인증하기',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white70,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

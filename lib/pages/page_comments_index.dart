import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/pages/page_enquiry_registration_index.dart';

class PageCommentsIndex extends StatefulWidget {
  const PageCommentsIndex({super.key});

  @override
  State<PageCommentsIndex> createState() => _PageCommentsIndexState();
}

class _PageCommentsIndexState extends State<PageCommentsIndex> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('문의하기',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromRGBO(55, 115, 78, 0.99),
      ),

      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(10),
          width: 500,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 16.0),
                    child: Text('총 (0건)의 문의내역',
                    style: TextStyle(
                      fontSize: 18,
                    ),),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 18.0),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                const PageEnquiryRegistrationIndex()));
                        // 버튼이 눌렸을 때 수행할 작업
                      },
                      child: Text('글쓰기',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                height: 300,
                child: Center(
                  child: Text("아직 문의 내역이 없어요 "),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

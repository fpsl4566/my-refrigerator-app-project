
import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/model/food_name_item.dart';
import 'package:my_refrigerator_app/repository/repo_food_name.dart';

/// Example without a datasource
class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}


List<FoodNameItem> _list = [];


class _PageIndexState extends State<PageIndex> {


  @override
  Future<void> _getList() async {
    print(_list);
    await RepoFoodName().getFoodNameListResult()
        .then((res) => {
      print(res),
      //성공
      setState(() {
      }),
    })
        .catchError((err) => {
      debugPrint(err)
      //실패
    });
  }

  @override
  void initState(){
    _getList();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(''),
      ),
      body: _build(context),
    );
  }
}

int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;

@override
Widget _build(BuildContext context) {
  return SingleChildScrollView(
    child: PaginatedDataTable(
      header: const Text('Nutrition'),
      rowsPerPage: _rowsPerPage,
      availableRowsPerPage: const <int>[5, 10, 20],
      columns: kTableColumns,
      source: DessertDataSource(),
    ),
  );
}

const kTableColumns = <DataColumn>[
  DataColumn(
    label: Text('재료목록'),
    numeric: true
  ),
];

class FoodNameItem {
  FoodNameItem(this.name);

  final String name;

  bool selected = false;
}

class DessertDataSource extends DataTableSource {
  int _selectedCount = 0;



  @override
  DataRow? getRow(int index) {
    assert(index >= 0);
    if (index >= _list.length) return null;
    final FoodNameItem dessert = _list[index];
    return DataRow.byIndex(
      index: index,
      selected: dessert.selected,
      onSelectChanged: (bool? value) {
        if (value == null) return;
        if (dessert.selected != value) {
          _selectedCount += value ? 1 : -1;
          assert(_selectedCount >= 0);
          dessert.selected = value;
          notifyListeners();
        }
      },
      cells: <DataCell>[
        DataCell(Text(dessert.name)),
      ],
    );
  }

  @override
  int get rowCount => _list.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}

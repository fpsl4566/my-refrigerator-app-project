import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/components/components_questions_item.dart';
import 'package:my_refrigerator_app/components/design/component_dropdown.dart';
import 'package:my_refrigerator_app/model/question_item.dart';
import 'package:my_refrigerator_app/repository/repo_questions.dart';

class PageQuestionsListIndex extends StatefulWidget {
  const PageQuestionsListIndex({
    super.key,
  });

  @override
  State<PageQuestionsListIndex> createState() => _PageQuestionsListIndexState();
}

class _PageQuestionsListIndexState extends State<PageQuestionsListIndex> {
  List<QuestionItem> _list = [];

  //가져 오는 함수 만들기
  @override
  Future<void> _loandList() async {
    await RepoQuestions()
        .getProducts()
        .then((res) => {
              print(res),
              //성공
              setState(() {
                _list = res.list;
              }),
            })
        .catchError((err) => {
              debugPrint(err)
              //실패
            });
  }

  @override
  void initState() {
    super.initState();
    _loandList();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('게시글 문의',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: const Color.fromRGBO(55, 115, 78, 0.99),
        ),

        body: SingleChildScrollView(

          child: Column(
            children: [Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    width: 1,
                    color: Colors.black
                  )
                )
              ),
              padding: EdgeInsets.all(10),

              child: Column(
                children: [
                  Row( mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(child: Text("[분류]",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18
                        ),)),
                      Container(margin: EdgeInsets.fromLTRB(120, 0, 0, 0),
                          width: 150,
                          child: Text("제목",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18
                            ),)),
                      Column(
                        children: [
                          Container(child: Text("아이디")),
                          Container(margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                              child: Text("작성일",
                                style: TextStyle(
                                    fontSize: 12
                                ),
                              )),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 20,)
                  //Text(questionItem.answerStatus),
                ],
              ),

            ),
              ListView.builder(
                shrinkWrap: true,
                itemCount: _list.length,
                // 리스트 아이템 개수
                itemBuilder: (BuildContext context, int index) {
                  ListTile(
                    title: Text('Item $index',
                    style: TextStyle(
                      fontSize: 15,color: Colors.grey,),
                  ),
                    onTap: (){

                      print('Tapped on item $index');
                    },
                  )
                  ;
                  SizedBox(
                    height: 10,

                  );


                  return ComponentsQuestionsItem(
                      questionItem: _list[index],
                      callback: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const PageQuestionsListIndex()));
                      });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

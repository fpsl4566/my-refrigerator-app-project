import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/components/components_refrigerator.dart';
import 'package:my_refrigerator_app/model/food_Basket_item_result.dart';
import 'package:my_refrigerator_app/pages/food/index2.dart';
import 'package:my_refrigerator_app/pages/food/page_food_imgs_index.dart';
import 'package:my_refrigerator_app/repository/repo_basket_refrigerator.dart';

class PageRefrigeratorFoodIndex extends StatefulWidget {
  const PageRefrigeratorFoodIndex({super.key});

  @override
  State<PageRefrigeratorFoodIndex> createState() =>
      _PageRefrigeratorFoodIndexState();
}

class _PageRefrigeratorFoodIndexState extends State<PageRefrigeratorFoodIndex> {
  final _scrollController = ScrollController();

  List<FoodBasketItemResult> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadList();
      }
    });

    _loadList();
  }


  Future<void> _loadList({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _list = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }

    if (_currentPage <= _totalPage) {
      await RepoRefrigerator().getRepoRefrigerator(page: _currentPage)
          .then((res) {
                setState(() {
                  _totalPage = res.totalPage.toInt();
                  _totalItemCount = res.totalCount.toInt();
                  _list = [..._list, ...res.list!];

                  _currentPage++;
                });
              }).catchError((err) {
                debugPrint(err);
              });
    }
    if (reFresh) {
      _scrollController.animateTo(
          0,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '담을 재료',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Color.fromRGBO(55, 115, 78, 0.99),
        centerTitle: true,
        elevation: 0.0,
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.menu),
        ),
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody(),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (BuildContext context, int idx) {
              return ComponentsRefrigerator(
                  foodBasketItemResult: _list[idx], callback: () {});
            }),
        SizedBox(
          height: 50.0,
          width: 40.0,
          child: TextButton(onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageFoodNameIndex2()));
          }, child: const Text('재료담기')),
        ),

      ],
    );
  }
}

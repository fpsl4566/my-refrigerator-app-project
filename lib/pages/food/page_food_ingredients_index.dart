import 'package:flutter/material.dart';

class PageFoodIngredientsIndex extends StatefulWidget {
  const PageFoodIngredientsIndex({super.key});

  @override
  State<PageFoodIngredientsIndex> createState() => _PageFoodIngredientsIndexState();
}

class _PageFoodIngredientsIndexState extends State<PageFoodIngredientsIndex> {

  final List<String> items = List.generate(20, (index) => '항목 $index');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('우리집 냉장고',
        style: TextStyle(
        color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromRGBO(55, 115, 78, 0.99),
      ),

      body: SafeArea(

        child: DefaultTabController(
          length: 3,
          child: SizedBox(
            height: 500,

            child: Column(
              children: [
                SizedBox(
                  height: 45,
                ),
                ListView.builder(
                    scrollDirection: Axis
                        .vertical, //vertical : 수직으로 나열 / horizontal : 수평으로 나열
                    itemCount: 0, //리스트의 개수
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        width: 100,
                        height: 10,
                        color: Colors.white,
                        alignment: Alignment.center,

                      );
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

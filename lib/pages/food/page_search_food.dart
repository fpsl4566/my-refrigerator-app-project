import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/components/components_search_food.dart';
import 'package:my_refrigerator_app/model/food_ingredient_item.dart';
import 'package:my_refrigerator_app/model/food_ingredient_list_result.dart';
import 'package:my_refrigerator_app/model/food_name_list_result.dart';
import 'package:my_refrigerator_app/model/order_of_cooking_item.dart';
import 'package:my_refrigerator_app/pages/food/Page_order_of_cooking_index.dart';
import 'package:my_refrigerator_app/repository/repo_food_ingredient.dart';
import 'package:my_refrigerator_app/repository/repo_order_of_cooking.dart';

class PageSearchFood extends StatefulWidget {
  const PageSearchFood({super.key,
  required this.id
  });

  final num id;

  @override
  State<PageSearchFood> createState() => _PageSearchFoodState();
}

class _PageSearchFoodState extends State<PageSearchFood> {
  List<FoodIngredientItem> _list = [];

  @override
  Future<void> _loandList() async {
    await RepoFoodIngredient()
        .getRepoFoodIngredient(widget.id)
        .then((res) => {
      //성공
      setState(() {
        _list = res.list;
      }),
    })
        .catchError((err) => {
      debugPrint(err)
      //실패
    });
    try{print(_list);} catch(e){ print(e);}
  }

  @override
  void initState() {
    super.initState();
    _loandList();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('레시피',
            style: TextStyle(
              color: Colors.white,

            ),
          ),
          backgroundColor: const Color.fromRGBO(55, 115, 78, 0.99),
        ),
        body: ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: _list.length, // 리스트 아이템 개수
          itemBuilder: (BuildContext context, int index) {
            return ComponentsSearchFood(
                callback: (){
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => PageOrderOfCookingIndex(id:_list[index].foodNameId)));

                }, foodIngredientItem: _list[index],);
          },
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/components/components_food_name_index.dart';
import 'package:my_refrigerator_app/model/food_name_item.dart';
import 'package:my_refrigerator_app/pages/food/Page_order_of_cooking_index.dart';
import 'package:my_refrigerator_app/repository/repo_food_name.dart';

class PageFoodImgsIndex extends StatefulWidget {
  const PageFoodImgsIndex({super.key});

  @override
  State<PageFoodImgsIndex> createState() => _PageFoodImgsIndexState();
}


List<FoodNameItem> _list = [];



class _PageFoodImgsIndexState extends State<PageFoodImgsIndex> {

  @override
  Future<void> _getList() async {
    print(_list);
    await RepoFoodName().getFoodNameListResult()
        .then((res) => {
      print(res),
      //성공
      setState(() {
        print(res);
        _list = res.list;
      }),
    })
        .catchError((err) => {
      debugPrint(err)
      //실패
    });
  }

  @override
  void initState(){
    _getList();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('레시피 종류',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor:const Color.fromRGBO(55, 115, 78, 0.99),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: ListView.builder(
          padding: const EdgeInsets.all(40.0),
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: _list.length,
          // 리스트 아이템 개수
          itemBuilder: (BuildContext context, int index) {
            ListTile(title: Text('Item $index',),
              onTap: () {

                print('Tapped on item $index');
              },
            );
            return ComponentsFoodName(
              foodNameItem: _list[index],
                callback: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  PageOrderOfCookingIndex(id:index+1)));
                },
            );
          },
        ),
      ),
    );
  }
}


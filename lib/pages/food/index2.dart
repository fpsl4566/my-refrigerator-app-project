import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/components/design/components_drawer_bar_normal.dart';
import 'package:my_refrigerator_app/components/design/components_refrigerator_food_index.dart';
import 'package:my_refrigerator_app/model/refrigerator_food_item.dart';
import 'package:my_refrigerator_app/pages/food/page_search_food.dart';
import 'package:my_refrigerator_app/repository/repo_refrigerator_food.dart';

class PageFoodNameIndex2 extends StatefulWidget {
  const PageFoodNameIndex2({super.key});

  @override
  State<PageFoodNameIndex2> createState() => _PageFoodNameIndexState();
}

List<RefrigeratorFoodItem> _list = [];
List<String> _list2=[
  'assets/beafcow.jpeg',
  'assets/Kimchi.jpg',
  'assets/egg1.jpg',
  'assets/Anchovy.jpg',

];


class _PageFoodNameIndexState extends State<PageFoodNameIndex2> {

  @override
  Future<void> _getList() async {
    print(_list);
    await RepoRefrigeratorFood().getRefrigeratorFood()
        .then((res) => {
      print(res),
      //성공
      setState(() {
        print(res);
        _list = res.list;
      }),
    })
        .catchError((err) => {
      debugPrint(err)
      //실패
    });
  }

  @override
  void initState(){
    _getList();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('담아둔 재료',
        style: TextStyle(
          color: Colors.white,
        ),
        ),
        backgroundColor: const Color.fromRGBO(55, 115, 78, 0.99),
      ),
      drawer: const ComponentsDrawerBarNormal(),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child:ListView.builder(
                physics: ScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: _list.length, //item 개수

                itemBuilder: (BuildContext context, int index)
                {
                return  ComponentsRefrigeratorFood(
                    refrigeratorFoodItem: _list[index],
                      refrigeratorFoodImg: _list2[index],
                      callback: (){
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PageSearchFood(id:_list[index].id)));
                      });
              }),
            ),Container( margin: EdgeInsets.fromLTRB(15, 0, 15, 15),
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 1,
                        color: Colors.black45,
                        style: BorderStyle.solid,
                      ),
                    ),
                  ),
                  child: Row(
                    children: [
                      Container(
                        height: 100,
                        width: 100,
                        child: Image.asset(
                          'assets/moo.jpeg',
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(width: 100),
                      Text(
                        "무",
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      );
  }
}

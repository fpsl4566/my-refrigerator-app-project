import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/components/components_order_of_cooking.dart';
import 'package:my_refrigerator_app/repository/repo_order_of_cooking.dart';

import '../../model/order_of_cooking_item.dart';

class PageOrderOfCookingIndex extends StatefulWidget {
  const PageOrderOfCookingIndex({super.key,
    required this.id
  });

  final num id;

  @override
  State<PageOrderOfCookingIndex> createState() =>
      _PageOrderOfCookingIndexState();
}



class _PageOrderOfCookingIndexState extends State<PageOrderOfCookingIndex> {
  List<OrderOfCookingItem> _list = [];


  @override
  Future<void> _loandList() async {
    await RepoOrderOfCooking()
        .getProducts(widget.id)
        .then((res) => {
              //성공
              setState(() {
                _list = res.list;
              }),
            })
        .catchError((err) => {
              debugPrint(err)
              //실패
            });
    try{print(_list);} catch(e){ print(e);}
  }

  @override
  void initState() {
    super.initState();
    _loandList();
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('레시피',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
          backgroundColor: const Color.fromRGBO(55, 115, 78, 0.99),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Container(
                width: 300,
                height: 300,
                child: Image.asset('assets/eggs.jpg',
                fit: BoxFit.cover
                ,),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: 
                Text('계란 말이',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold
                ),
                )
                ),
              ListView.builder(
                padding: EdgeInsets.all(8),
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: _list.length, // 리스트 아이템 개수
                itemBuilder: (BuildContext context, int index) {
                  return ComponentsOrderOfCooking(
                      callback: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PageOrderOfCookingIndex(id:index)));
                      },indexOfCooking: [index+1],
                    orderOfCookingItem: _list[index],);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

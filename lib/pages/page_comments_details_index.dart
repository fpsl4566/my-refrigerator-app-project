import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/pages/page_enquiry_registration_index.dart';

class PageCommentsDetailsIndex extends StatefulWidget {
  const PageCommentsDetailsIndex({super.key});

  @override
  State<PageCommentsDetailsIndex> createState() =>
      _PageCommentsDetailsIndexState();
}

class _PageCommentsDetailsIndexState extends State<PageCommentsDetailsIndex> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '문의내역',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromRGBO(55, 115, 78, 0.99),
      ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(10),
          width: 500,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 16.0),
                    child: Text(
                      '총 (1건)의 문의내역',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 18.0),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                const PageEnquiryRegistrationIndex()));
                        // 버튼이 눌렸을 때 수행할 작업
                      },
                      child: Text(
                        '글쓰기',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Container(
                              margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                              padding: EdgeInsets.all(20),
                              child: Text(
                                "[문의사항 (레시피,냉장고재료,기타)] 재료]",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18),
                              )),
                          Container(
                              child: Text(
                            '2024-03-04',
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          )),
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                              child: Text(
                            '미답변',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                            ),
                          )),
                        ],
                      ),
                    ],
                  ),
                  //Text(questionItem.answerStatus),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class CommentInputWidget extends StatefulWidget {
  final void Function(String text, String author) onSubmit;

  CommentInputWidget({required this.onSubmit});

  @override
  _CommentInputWidgetState createState() => _CommentInputWidgetState();
}

class _CommentInputWidgetState extends State<CommentInputWidget> {
  final TextEditingController _textEditingController = TextEditingController();
  final TextEditingController _authorEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: _textEditingController,
          decoration: InputDecoration(labelText: '댓글 내용'),
        ),
        TextField(
          controller: _authorEditingController,
          decoration: InputDecoration(labelText: '작성자'),
        ),
        ElevatedButton(
          onPressed: () {
            widget.onSubmit(_textEditingController.text, _authorEditingController.text);
            _textEditingController.clear();
            _authorEditingController.clear();
          },
          child: Text('전송'),
        ),
      ],
    );
  }
}
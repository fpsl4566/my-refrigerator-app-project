import 'package:flutter/material.dart';

class PageQuestionsDetailsIndex extends StatefulWidget {
  const PageQuestionsDetailsIndex({super.key});

  @override
  State<PageQuestionsDetailsIndex> createState() => _PageQuestionsDetailsIndexState();
}

class _PageQuestionsDetailsIndexState extends State<PageQuestionsDetailsIndex> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('게시글 상세보기'),
      ),
      body: SingleChildScrollView(

        child: Column(
          children: [Container(
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                        width: 1,
                        color: Colors.black
                    )
                )
            ),
            padding: EdgeInsets.all(10),

            child: Column(
              children: [
                Row( mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(child: Text("[분류]",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18
                      ),)),
                    Container(margin: EdgeInsets.fromLTRB(120, 0, 0, 0),
                        width: 150,
                        child: Text("제목",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18
                          ),)),
                    Column(
                      children: [
                        Container(child: Text("아이디")),
                        Container(margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("작성일",
                              style: TextStyle(
                                  fontSize: 12
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 20,)
                //Text(questionItem.answerStatus),
              ],
            ),

          ),
            ListView.builder(
              shrinkWrap: true,
              itemCount: _list.length,
              // 리스트 아이템 개수
              itemBuilder: (BuildContext context, int index) {
                ListTile(
                  title: Text('Item $index',
                    style: TextStyle(
                      fontSize: 15,color: Colors.grey,),
                  ),
                  onTap: (){

                    print('Tapped on item $index');
                  },
                )
                ;
                SizedBox(
                  height: 10,

                );


                return (
                    questionItem: _list[index],
                    callback: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const PageQuestionsListIndex()));
                    });
              },
            ),
          ],
        ),
      ),,
    );
  }
}

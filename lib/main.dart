import 'package:flutter/material.dart';
import 'package:my_refrigerator_app/pages/food/index2.dart';
import 'package:my_refrigerator_app/pages/mainpage/Page_main_index.dart';
import 'package:my_refrigerator_app/pages/page_comments_details_index.dart';
import 'package:my_refrigerator_app/pages/page_index.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor:Color.fromRGBO(55, 115, 78, 0.99)),
        useMaterial3: true,
      ),
      home: PageMainIndex()
    );
  }
}



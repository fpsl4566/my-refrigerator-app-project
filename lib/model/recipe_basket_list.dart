import 'package:my_refrigerator_app/model/order_of_cooking_item.dart';

class RecipeBasketList{
String msg;
num code;
List<OrderOfCookingItem>list;
num totalCount;

RecipeBasketList( this.msg,this.code,this.list,this.totalCount);

factory RecipeBasketList.fromJson(Map<String,dynamic>json){
  return RecipeBasketList(
      json['msg'],
      json['code'],
      json['list']  !=null ? (json['list'] as List).map((e) => OrderOfCookingItem.fromJson(e)).toList() :[],
      json['totalCount']);
}
}

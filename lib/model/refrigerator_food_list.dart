
import 'package:my_refrigerator_app/model/refrigerator_food_item.dart';


class RefrigeratorFoodListResult{

  num code;
  List<RefrigeratorFoodItem> list;
  num totalCount;


  RefrigeratorFoodListResult(this.code,this.list,this.totalCount );

  factory RefrigeratorFoodListResult.fromJson(Map<String,dynamic>json){
    return RefrigeratorFoodListResult(
        json['code'],
        json['list'] != null ? (json['list'] as List).map((e) => RefrigeratorFoodItem.fromJson(e)).toList() : [],
        json['totalCount'],
    );
  }
}
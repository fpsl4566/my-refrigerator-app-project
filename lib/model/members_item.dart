class MembersItem {

  num id;
  String memberName;
  String memberId;
  String password;
  String phoneNumber;
  String mail;

  MembersItem(this.id, this.memberName, this.memberId, this.password,this.mail,this.phoneNumber);


  factory MembersItem.fromJson(Map<String, dynamic> json) {
    return MembersItem(
      json['id'],
      json['memberName'],
      json['memberId'],
      json['password'],
      json['phoneNumber'],
      json['mail'],

    );

  }
}

class QuestionsDuestionsItem{
  num id;
  String memberEntityId;
  String questionCategory;
  String title;
  String questionContents;
  //bool answerStatus;
  String dateWrite;

  QuestionsDuestionsItem(this.id,this.memberEntityId,this.title,this.questionContents,this.questionCategory,this.dateWrite);

  factory QuestionsDuestionsItem.fromJson(Map<String,dynamic>json){
    return QuestionsDuestionsItem(
      json['id'],
      json['memberEntityId'],
      json['questionCategory'],
      json['title'],
      json['questionContents'],
      //json['answerStatus'],
      json['dateWrite'],
    );
  }

}
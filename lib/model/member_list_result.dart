import 'package:my_refrigerator_app/model/members_item.dart';

class MemberListResult {
  String msg;
  num code;
  List<MembersItem> list;
  num totalCount;
  num totalPage;
  num currentPage;

  MemberListResult(this.msg, this.code, this.list, this.totalCount,
      this.currentPage, this.totalPage);

  factory MemberListResult.fromJson(Map<String, dynamic> json) {
    return MemberListResult(
        json['msg'],
        json['code'],
        json['list'] != null
            ? (json['list'] as List)
                .map((e) => MembersItem.fromJson(e))
                .toList()
            : [],
        json['totalCount'],
        json['totalPage'],
        json['currentPage']);
  }
}

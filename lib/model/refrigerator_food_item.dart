import 'package:dio/dio.dart';


class RefrigeratorFoodItem {
  num id;
  String refrigeratorFood;
  String refrigeratorName;


  RefrigeratorFoodItem(this.id, this.refrigeratorFood,
       this.refrigeratorName,
      );

  factory RefrigeratorFoodItem.fromJson(Map<String, dynamic> json) {
    return RefrigeratorFoodItem(
        json['id'] ,
        json['refrigeratorFood'] ,
        json['refrigeratorName'] ,

    );

  }
}



import 'package:my_refrigerator_app/model/question_item.dart';

class QuestionListResult{
  String msg;
  num code;
  List<QuestionItem>list;
  num totalCount;



  QuestionListResult(this.msg,this.code,this.list,this.totalCount);

  factory QuestionListResult.fromJson(Map<String,dynamic>json){
    return QuestionListResult(
        json['msg'],
        json['code'],
        json['list'] != null ? (json['list'] as List).map((e) => QuestionItem.fromJson(e)).toList() : [],
        json['totalCount'],
    );
  }
}
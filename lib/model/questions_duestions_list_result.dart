
import 'dart:core';

class QuestionsDuestionsListResult{
  String msg;
  num code;
  List<QuestionsDuestionsListResult>List;

  QuestionsDuestionsListResult(this.msg,this.code,this.List);

  factory QuestionsDuestionsListResult.fromJson(Map<String,dynamic>json){
    return QuestionsDuestionsListResult(
        json['msg'],
        json['code'],
        json['List'] != null ? (json['list'] as List).map((e) => QuestionsDuestionsListResult.fromJson(e)).toList() : [],
    );
  }
}
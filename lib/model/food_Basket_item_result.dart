class FoodBasketItemResult {
  String refrigeratorFood;
  String dateRefrigeratorFood;

  FoodBasketItemResult(this.refrigeratorFood,  this.dateRefrigeratorFood);

  factory FoodBasketItemResult.fromJson(Map<String, dynamic> json) {
    return FoodBasketItemResult(
        json['refrigeratorFood'],
        json['dateRefrigeratorFood']);
  }
}

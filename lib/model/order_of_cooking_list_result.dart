import 'package:my_refrigerator_app/model/order_of_cooking_item.dart';

class OrderOfCookingListResult {

  num code;
  List<OrderOfCookingItem> list;
  num totalCount;


  OrderOfCookingListResult( this.code, this.list, this.totalCount);

  factory OrderOfCookingListResult.fromJson(Map<String, dynamic> json) {
    return OrderOfCookingListResult(
      json['code'],
      json['list'] != null
          ? (json['list'] as List)
              .map((e) => OrderOfCookingItem.fromJson(e))
              .toList()
          : [],
      json['totalCount'],
    );
  }
}

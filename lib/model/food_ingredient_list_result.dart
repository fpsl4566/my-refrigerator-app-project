import 'package:my_refrigerator_app/model/food_ingredient_item.dart';

class FoodIngredientListResult {

  List<FoodIngredientItem> list;
  num totalCount;

  FoodIngredientListResult( this.list, this.totalCount);

  factory FoodIngredientListResult.fromJson(Map<String, dynamic>json) {
    return FoodIngredientListResult(

      json['list'] != null? (json['list'] as List).map((e) => FoodIngredientItem.fromJson(e)).toList()
          : [],
      json['totalCount'],

    );
  }
}


import 'package:my_refrigerator_app/model/food_name_item.dart';


class FoodNameListResult{
  String msg;
  num code;
  List<FoodNameItem>list;
  num totalCount;

  FoodNameListResult(this.msg,this.code,this.list,this.totalCount);

  factory FoodNameListResult.fromJson(Map<String,dynamic>json){
    return FoodNameListResult(
        json['msg'],
        json['code'],
        json['list'] != null ? (json['list'] as List).map((e) => FoodNameItem.fromJson(e)).toList() : [],
        json['totalCount']
    );
  }
}
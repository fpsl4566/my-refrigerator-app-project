import 'package:my_refrigerator_app/model/food_Basket_item_result.dart';

class FoodBasketListResult {

  List<FoodBasketItemResult> list;
  num totalCount;
  num totalPage;
  num currentPage;

  FoodBasketListResult(  this.list , this.totalCount, this.totalPage , this.currentPage);

  factory FoodBasketListResult.fromJson(Map<String, dynamic> json) {
    return FoodBasketListResult(
        json['list'] != null ? (json['list'] as List).map((e) => FoodBasketItemResult.fromJson(e)).toList() : [],
        json['totalCount'],
        json['totalPage'],
        json['currentPage']);
  }
}

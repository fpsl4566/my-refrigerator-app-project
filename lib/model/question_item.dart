class QuestionItem{

  num id;
  String memberEntityId;
  String questionCategory;
  String title;
  String questionContents;
  //bool answerStatus;
  String dateWrite;


  QuestionItem(this.id, this.memberEntityId, this.questionCategory, this.title,this.questionContents,this.dateWrite);


  factory QuestionItem.fromJson(Map<String, dynamic> json) {
    return QuestionItem(
      json['id'],
      json['memberEntityId'],
      json['questionCategory'],
      json['title'],
      json['questionContents'],
      //json['answerStatus'],
      json['dateWrite'],

    );


  }
}
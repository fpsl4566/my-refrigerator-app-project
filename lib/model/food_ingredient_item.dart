import 'package:my_refrigerator_app/model/food_name_list_result.dart';

class FoodIngredientItem{

  int foodNameId;
  String foodName;
  String refrigeratorFood;

  FoodIngredientItem(this.foodNameId,this.foodName,this.refrigeratorFood);

  factory FoodIngredientItem.fromJson(Map<String,dynamic>json){
    return FoodIngredientItem(
      json['foodNameId'],
      json['foodName'],
      json['refrigeratorFood'],
    );
  }

}
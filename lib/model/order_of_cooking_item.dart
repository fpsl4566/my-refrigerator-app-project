
class OrderOfCookingItem{
  num? id;
  num? foodNameId;
  String? recipe;


  OrderOfCookingItem(this.id, this.foodNameId, this.recipe);

  factory OrderOfCookingItem.fromJson(Map<String,dynamic> json){
    return OrderOfCookingItem(
    json['id'],
    json['foodNameId'] ,
    json['recipe']
    );
  }
}
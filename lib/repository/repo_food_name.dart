import 'package:dio/dio.dart';
import 'package:my_refrigerator_app/config/config_api.dart';
import 'package:my_refrigerator_app/model/food_name_list_result.dart';


class RepoFoodName{
  Future<FoodNameListResult> getFoodNameListResult() async{
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/food-name/all';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status){
              return status == 200;

            }
        )
    );


    return FoodNameListResult.fromJson(response.data);
  }
}

import 'package:dio/dio.dart';
import 'package:my_refrigerator_app/config/config_api.dart';
import 'package:my_refrigerator_app/model/order_of_cooking_list_result.dart';

class RepoOrderOfCooking{
  Future<OrderOfCookingListResult> getProducts(num foodNameId)  async{
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/order-of-cooking/all/{foodNameId}';

    final response = await dio.get(
      _baseUrl.replaceAll('{foodNameId}', foodNameId.toString()),
      options: Options(
        followRedirects: false,
        validateStatus:  (status){
          return status == 200;
        }
      )
    );
    print(response.data);
    return OrderOfCookingListResult.fromJson(response.data);
  }

  Future<OrderOfCookingListResult> getRecipe(num refrigeratorId)  async{
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/food-ingredient/refrigerator-food-id/{refrigeratorId}';

    final response = await dio.get(
        _baseUrl.replaceAll('{refrigeratorId}', refrigeratorId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus:  (status){
              return status == 200;
            }
        )
    );
    print(response.data);
    return OrderOfCookingListResult.fromJson(response.data);
  }

}
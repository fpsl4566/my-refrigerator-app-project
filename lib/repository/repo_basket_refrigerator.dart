import 'package:dio/dio.dart';
import 'package:my_refrigerator_app/config/config_api.dart';
import 'package:my_refrigerator_app/model/food_Basket_list_result.dart';


class RepoRefrigerator{
  Future<FoodBasketListResult> getRepoRefrigerator({int page = 1}) async{
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/refrigerator_food/all/{pageNum}'.replaceAll('{pageNum}', page.toString());

    final response = await dio.get(
        _baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status){
              return status == 200;
            }
        )
    );
    print(response.data);
    return FoodBasketListResult.fromJson(response.data);
  }
} //재료 담는 페이지
import 'package:flutter/material.dart';

class PageWritingIndex extends StatefulWidget {
  const PageWritingIndex({super.key});

  @override
  State<PageWritingIndex> createState() => _PageWritingIndexState();
}

class _PageWritingIndexState extends State<PageWritingIndex> {
  List<String> comments = [
    "첫 번째 댓글",
    "두 번째 댓글",
    "세 번째 댓글",
    "네 번째 댓글",
    // 여러 댓글 추가 가능
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('댓글'),
      ),
      body: ListView.builder(
        itemCount: comments.length,
        itemBuilder: (context, index) {
          return CommentWidget(comment: comments[index]);
        },
      ),
    );
  }
}

class CommentWidget extends StatelessWidget {
  final String comment;

  const CommentWidget({Key? key, required this.comment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Text(comment),
    );
  }
}

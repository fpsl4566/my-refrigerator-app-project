
import 'package:dio/dio.dart';
import 'package:my_refrigerator_app/config/config_api.dart';
import 'package:my_refrigerator_app/model/food_ingredient_list_result.dart';
import 'package:my_refrigerator_app/model/food_name_list_result.dart';

class RepoFoodIngredient{
  Future<FoodIngredientListResult>getRepoFoodIngredient(num refrigeratorId)async{
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/food-ingredient/refrigerator-food-id/{refrigeratorId}';

    final response = await dio.get(
      _baseUrl.replaceAll('{refrigeratorId}', refrigeratorId.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status){
          return status == 200;
        }
      )
    );
    print(response.data);
    return FoodIngredientListResult.fromJson(response.data);
  }
}
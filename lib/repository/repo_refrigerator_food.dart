
import 'package:dio/dio.dart';
import 'package:my_refrigerator_app/config/config_api.dart';
import 'package:my_refrigerator_app/model/refrigerator_food_list.dart';

class RepoRefrigeratorFood{
  Future<RefrigeratorFoodListResult> getRefrigeratorFood() async {
    Dio dio = Dio();
    print(1);
    final String _baseUrl = "$apiUri/refrigerator/all";
    print(2);
    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status){
              return status == 200;
            }
        )
    );
    print(response.data);
    return RefrigeratorFoodListResult.fromJson(response.data);
  }
}//완료 !!!
import 'package:dio/dio.dart';
import 'package:my_refrigerator_app/config/config_api.dart';
import '../model/member_list_result.dart';

class RepoProduct {
  Future<MemberListResult> getProduct(num foodNameId) async {
    Dio dio = Dio();


    final String _baseUrl = '$apiUri/product/all/{foodNameId}';

    final response = await dio.get(
        _baseUrl.replaceAll('{foodNameId}', foodNameId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return MemberListResult.fromJson(response.data);
  }
  }

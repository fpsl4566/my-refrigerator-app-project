
import 'package:dio/dio.dart';
import 'package:my_refrigerator_app/config/config_api.dart';
import 'package:my_refrigerator_app/model/question_list_result.dart';


class RepoQuestions{
  Future<QuestionListResult> getProducts() async{
    Dio dio = Dio(); //빈생성자 호출

    final String _baseUrl = '$apiUri/questions/all'; //앤드포인트
    //const도 사용이 가능하나 파일을 임포드해오기에 늦어지면 다 불러와지지 못하는 현 상이 생겨서 const보단 final
    //올 때까지 기다렸다가 들어오면 마감

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false, //다른 연결 하는 곳으로 갈거먀 말거냐 다른 곳으로 규칙이 바뀌니 안간다는 뜻
            validateStatus: (status){ //전화연결상태
              return status == 200; //정상이면 200 성공
            }
        )
    );
    print(response.data);
    return QuestionListResult.fromJson(response.data);
  }
}